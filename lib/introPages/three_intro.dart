import 'package:flutter/material.dart';
import 'package:karnak_scan_project/auth_screen/login_screen.dart';





var assetImage = AssetImage('assets/images/third-intro-pic.png');
var slideImg = AssetImage('assets/images/noun_dots_3.png');
var baseColor = Color(0xffECF1FA); //backgroundColor
var redColor = Color(0xffE23C3C); //redColor
var bluecolor = Color(0xff181461); //blueColor

var image = Image(
  image: assetImage,
);

var slideImage = Image(
  image: slideImg,
);


class ThirdIntro extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      backgroundColor: baseColor,
      body: Stack(
        children: [
          Container(
              margin: EdgeInsets.only(top: 70, left: 105),
              height: 150.0,
              width: 150.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                      'assets/images/logo-v2.png'),
                  fit: BoxFit.fill,
                ),
              )

          ),
          //Karnak Scan Title


          //Sub Title


          //Discus in
          Container(
              color: baseColor,
              margin: EdgeInsets.only(top: 250, left: 125,),
              child: Text.rich(
                  TextSpan(children: <TextSpan>[
                    TextSpan(
                      text: "Discuss",
                      style: TextStyle(color: bluecolor, fontSize: 24),
                    ),


                    TextSpan(
                      text: " in ",
                      style: TextStyle(color: bluecolor, fontSize: 24),
                    ),


                  ])
              )
          ),

          //in the Fourm
          Container(
              color: baseColor,
              margin: EdgeInsets.only(top: 280, left: 125,),
              child: Text.rich(
                  TextSpan(children: <TextSpan>[

                    TextSpan(
                      text: "the Fourm",
                      style: TextStyle(color: bluecolor, fontSize: 24),
                    ),
                  ])
              )
          ),

          //image
          Container(

            color: baseColor,
            height: 300,
            width: 360,
            child: image,
            margin: EdgeInsets.only(top: 305, left: 0),
          ),

          //Next Button
          Container(

            // ignore: deprecated_member_use
            child:RaisedButton(

              elevation: 0,
              hoverElevation: 0,
              focusElevation: 0,
              highlightElevation: 0,

              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context)=>LoginScreen()),
                );
              },
              color: baseColor,
              child: Text("Next",style:  TextStyle(color: redColor),),
            ),

            color: baseColor,
            margin: EdgeInsets.only(top:720, left: 275),

          ),

          //Skip Button
          Container(

            // ignore: deprecated_member_use
            child:RaisedButton(

              elevation: 0,
              hoverElevation: 0,
              focusElevation: 0,
              highlightElevation: 0,

              onPressed: () {
                print("Hello Joe");
              },
              color: baseColor,
              child: Text("Skip",style: TextStyle(color: redColor),),
            ),

            color: baseColor,
            margin: EdgeInsets.only(top: 720, right: 0),


          ),

          //Slide Image
          Container(

            height: 30,
            width: 50,
            child: slideImage,
            margin: EdgeInsets.only(top: 725, left: 155),
          ),


        ],
      ),

    );
  }
}