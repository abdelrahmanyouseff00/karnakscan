
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';



var baseColor = Color(0xffECF1FA); //backgroundColor
var redColor = Color(0xffE23C3C); //redColor
var bluecolor = Color(0xff181461);

class Home extends StatelessWidget {
  const Home({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 90,
          title: Column(
            children: <Widget>[
              Text(
                'Karnak',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 30,
                ),
              ),
              Text(
                'Scan & Lab',
                style: TextStyle(fontSize: 14),
              )
            ],
          ),
          backgroundColor: Color(0xffE23C3C),
          centerTitle: true,

        ),

        drawer: Drawer(
          child: ListView(
              padding: EdgeInsets.all(0.0),
              children: <Widget>[
                UserAccountsDrawerHeader(
                  accountName: Text('Abdelrahman Yousef'),
                  accountEmail: Text('abdelrahmanyouseff@gmail.com'),
                  currentAccountPicture: CircleAvatar(
                    backgroundImage: ExactAssetImage('assets/images/aleydon.jpg'),
                  ),

                  // otherAccountsPictures: <Widget>[
                  //   CircleAvatar(
                  //     child: Text('A'),
                  //     backgroundColor: Colors.white60,
                  //     ),
                  //     CircleAvatar(
                  //         child: Text('R'),
                  //     ),
                  //   ],

                  onDetailsPressed: (){},
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/images/menu.png"),
                          fit: BoxFit.cover)
                  ),),

                ListTile(
                    title: Text('Profile'),
                    leading: Icon(Icons.person),
                    onLongPress: (){
                      // Navigator.push(context,
                      //   MaterialPageRoute(builder: (context)=>Profile()),
                      // );
                    }
                ),

                Divider(),
                ListTile(
                  title: Text('X-Ray Results'),
                  leading: Icon(Icons.group_add),

                ),

                ListTile(
                  title: Text('New Appointment'),
                  leading: Icon(Icons.chat),
                  onLongPress: (){},
                ),


                ListTile(
                  title: Text('Medical Records'),
                  leading: Icon(Icons.call),
                  onLongPress: (){},
                ),


                Divider(),


                ListTile(
                  title: Text('Fourm'),
                  leading: Icon(Icons.settings),
                  onLongPress: (){},
                ),

                ListTile(
                  title: Text('Statistics'),
                  leading: Icon(Icons.report_problem),
                  onLongPress: (){},
                ),

                Divider(),




                ListTile(
                    title: Text('Settings'),
                    leading: Icon(Icons.close),
                    onTap: (){
                      Navigator.of(context).pop();}
                ),

                ListTile(
                    title: Text('Help'),
                    leading: Icon(Icons.close),
                    onTap: (){
                      Navigator.of(context).pop();}
                ),

                ListTile(
                    title: Text('About us'),
                    leading: Icon(Icons.close),
                    onTap: (){
                      Navigator.of(context).pop();}
                ),

                ListTile(
                    title: Text('Logout'),
                    leading: Icon(Icons.close),
                    onTap: (){
                      Navigator.of(context).pop();}
                ),
              ]
          ),
        ),


        body: Stack(
          children: [

            //redContainer

            // //Hello
            //    Container(
            //   child: Container(
            //      color: redColor,
            //     margin: EdgeInsets.only(top: 150, left:20),
            //     child: AnimatedTextKit(
            //       animatedTexts: [
            //       TypewriterAnimatedText(
            //       'Hello!',

            //       textStyle: const TextStyle(
            //         color: Colors.white,
            //       fontSize: 20.0,
            //       fontWeight: FontWeight.bold,
            //     ),
            //     speed: const Duration(milliseconds: 50),
            //      ),
            //     ],
            //     )
            //   ),
            // ),

            // // Abdelrahman
            // Container(
            //   child: Container(
            //      color: redColor,
            //     margin: EdgeInsets.only(top: 170, left:20),
            //     child: AnimatedTextKit(
            //       animatedTexts: [
            //       TypewriterAnimatedText(
            //       'Abdelrahman!',

            //       textStyle: const TextStyle(
            //         color: Colors.white,
            //       fontSize: 20.0,
            //       fontWeight: FontWeight.bold,
            //     ),
            //     speed: const Duration(milliseconds: 50),
            //      ),
            //     ],
            //     )
            //   ),
            // ),







            // Container(
            //   color: Colors.white,
            //   margin: EdgeInsets.only(top: 320, left: 20),
            //   child: Text.rich(
            //     TextSpan(children: <TextSpan>[
            //       TextSpan(

            //           text: "Our Service",
            //           style: TextStyle(color: Colors.black, fontSize: 25, fontWeight: FontWeight.bold)
            //       ),
            //     ])
            //   ),
            // ),

            Container(
              color: Colors.white,
              margin: EdgeInsets.only(top: 0),
              padding: EdgeInsets.only(top: 0),
              height: 220,
              child: ListView(
                children: [
                  SizedBox(
                    height: 200.0,
                    width: double.maxFinite,
                    child: Carousel(
                      images: [
                        Image.asset("assets/images/serv-slide-01.png"),
                        Image.asset("assets/images/serv-slide-02.png"),
                        Image.asset("assets/images/serv-slide-03.png"),
                      ],
                    ),
                  )
                ],
              ),

            ),
            Container(

                height: 190,
                margin: EdgeInsets.only(top: 390, left: 15),
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    //service 1
                    GestureDetector(
                      onTap: (){

                      },
                      child: Container(
                        width: 120,
                        child: Card(
                          child: Wrap(
                            children: <Widget>[
                              Image.asset('assets/images/srv-01.png'),

                            ],
                          ),
                        ),
                      ),
                    ),

                    //service 2
                    Container(
                      width: 120,
                      child: Card(
                        child: Wrap(
                          children: <Widget>[
                            Image.asset('assets/images/srv-02.png'),

                          ],
                        ),
                      ),
                    ),

                    //service 3
                    Container(
                      width: 120,
                      child: Card(
                        child: Wrap(
                          children: <Widget>[
                            Image.asset('assets/images/srv-03.png'),

                          ],
                        ),
                      ),
                    ),

                    //service 4
                    Container(
                      width: 120,
                      child: Card(
                        child: Wrap(
                          children: <Widget>[
                            Image.asset('assets/images/srv-04.png'),

                          ],
                        ),
                      ),
                    ),

                    //service 5
                    Container(
                      width: 120,
                      child: Card(
                        child: Wrap(
                          children: <Widget>[
                            Image.asset('assets/images/srv-05.png'),

                          ],
                        ),
                      ),
                    ),

                    //service 6
                    Container(
                      width: 120,
                      child: Card(
                        child: Wrap(
                          children: <Widget>[

                            Image.asset('assets/images/srv-06.png'),

                          ],
                        ),
                      ),
                    ),

                    //service 7
                    Container(
                      width: 120,
                      child: Card(
                        child: Wrap(
                          children: <Widget>[
                            Image.asset('assets/images/srv-07.png'),

                          ],
                        ),
                      ),
                    ),

                    Container(
                      width: 120,
                      child: Card(
                        child: Wrap(
                          children: <Widget>[
                            Image.asset('assets/images/srv-08.png'),

                          ],
                        ),
                      ),
                    ),


                    Container(
                      width: 120,
                      child: Card(
                        child: Wrap(
                          children: <Widget>[
                            Image.asset('assets/images/srv-09.png'),

                          ],
                        ),
                      ),
                    ),


                    Container(
                      width: 120,
                      child: Card(
                        child: Wrap(
                          children: <Widget>[
                            Image.asset('assets/images/srv-10.png'),

                          ],
                        ),
                      ),
                    ),


                    Container(
                      width: 120,
                      child: Card(
                        child: Wrap(
                          children: <Widget>[
                            Image.asset('assets/images/srv-11.png'),

                          ],
                        ),
                      ),
                    ),


                    Container(
                      width: 120,
                      child: Card(
                        child: Wrap(
                          children: <Widget>[
                            Image.asset('assets/images/srv-12.png'),

                          ],
                        ),
                      ),
                    ),


                    Container(
                      width: 120,
                      child: Card(
                        child: Wrap(
                          children: <Widget>[
                            Image.asset('assets/images/srv-13.png'),

                          ],
                        ),
                      ),
                    ),


                    Container(
                      width: 120,
                      child: Card(
                        child: Wrap(
                          children: <Widget>[
                            Image.asset('assets/images/srv-14.png'),

                          ],
                        ),
                      ),
                    ),

                  ],
                )

            ),
          ],
        )
    );
  }
}