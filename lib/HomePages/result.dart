import 'package:flutter/material.dart';


class Results extends StatefulWidget {
  const Results({Key key}) : super(key: key);

  @override
  _ResultsState createState() => _ResultsState();
}

class _ResultsState extends State<Results> {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: DefaultTabController(
          length: 2,
          child: Scaffold(
            appBar: AppBar(
              toolbarHeight: 200,
              backgroundColor: Colors.white,
              bottom: TabBar(
                labelColor: Color(0xffE23C3C),
                unselectedLabelColor: Colors.grey,
                indicatorColor: Color(0xffE23C3C),

                tabs: <Widget>[
                  Tab(

                    text: 'Recently',
                  ),
                  Tab(
                    text: 'Past',
                  ),
                ],
              ),
            ),

          )

      ),


    );
  }
}