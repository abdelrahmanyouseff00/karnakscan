import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class ForumResult extends StatefulWidget {
  const ForumResult({Key key}) : super(key: key);

  @override
  _ForumResultState createState() => _ForumResultState();
}

class _ForumResultState extends State<ForumResult> {
  double height, width;

  @override
  Widget build(BuildContext context) {
    if (MediaQuery.of(context).orientation == Orientation.portrait) {
      height = MediaQuery.of(context).size.height;
      width = MediaQuery.of(context).size.width;
    } else {
      height = MediaQuery.of(context).size.width;
      width = MediaQuery.of(context).size.height;
    }

    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            ListView.builder(
              // physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: 15,
              itemBuilder: (context,index){
                return Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("09/04/2021",style: TextStyle(color: Colors.grey,fontSize: 12),),
                              SizedBox(height: 4,),
                              Text("Magnetect - claro",style: TextStyle(fontWeight: FontWeight.bold),),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Row(
                                children: [
                                  Icon(Icons.chat,color: Colors.grey,size: 20,),
                                  SizedBox(width: 2,),
                                  Text("20",style: TextStyle(fontSize: 12,color: Colors.grey),)
                                ],
                              ),
                              SizedBox(height: 2,),
                              Row(
                                children: [
                                  Text("See more",style: TextStyle(color: HexColor('#dd3f36')
                                      ,fontWeight: FontWeight.bold),),
                                  Icon(Icons.arrow_forward_ios,color: HexColor('#dd3f36')
                                    ,size: 20,),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),

                      Divider(
                        thickness: 1,
                        color: Colors.grey,
                      ),
                      SizedBox(height: 6,),
                    ],
                  ),
                );
              },),
            SizedBox(height: 20,),
            Container(
              alignment: Alignment.center,
              width: width,
              height: 50,
              decoration: BoxDecoration(
                color: HexColor('#dd3f36'),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Text("Book a new request",style: TextStyle(
                  color: Colors.white
              ),),
            )
          ],
        ),
      ),
    );
  }
}
