import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:hexcolor/hexcolor.dart';


class BookAppointment extends StatefulWidget {
  const BookAppointment({Key key}) : super(key: key);

  @override
  _BookAppointmentState createState() => _BookAppointmentState();
}

class _BookAppointmentState extends State<BookAppointment> {

  double height, width;
  final focus = FocusNode();

  final TextEditingController email = TextEditingController();

  String _email;

  @override
  Widget build(BuildContext context) {
    if (MediaQuery.of(context).orientation == Orientation.portrait) {
      height = MediaQuery.of(context).size.height;
      width = MediaQuery.of(context).size.width;
    } else {
      height = MediaQuery.of(context).size.width;
      width = MediaQuery.of(context).size.height;
    }
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: HexColor('#eeeff9'),
         // padding: EdgeInsets.symmetric(horizontal: 16,vertical: 16),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
               Container(
                 padding: EdgeInsets.symmetric(horizontal: 16,vertical: 16),
                 color: Colors.white,
                 child: Column(
                   mainAxisAlignment: MainAxisAlignment.start,
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                     Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       children: [
                         Icon(Icons.notes,color: HexColor('#dd3f36'),),
                         Icon(Icons.person,color: HexColor('#dd3f36'),),
                       ],
                     ),
                     SizedBox(height: 10,),
                     Text("Book an appointment",style: TextStyle(fontSize: 20
                       ,fontWeight: FontWeight.bold,color: HexColor('#dd3f36'),),),
                     SizedBox(height: 20,),
                   ],
                 ),
               ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20,vertical: 16),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: TextFormField(
                          controller: email,
                          onChanged: ((String password) {
                            setState(() {
                              _email = password;
                            });
                          }),
                          focusNode: focus,
                          validator: (value) {
                            if (value.isEmpty) {
                              return "";
                            }
                            if (value.length < 6) {
                              return " ";
                            } else
                              return null;
                          },
                          //obscureText: true,
                          autofocus: false,
                          decoration: InputDecoration(
                            hintText: "Doctor,Specialities",
                            hintStyle: TextStyle(fontSize: 14),
                            prefixIcon: Icon(Icons.search,color: HexColor('#dd3f36'),),
                            filled: true,
                            fillColor: HexColor("#FFFFFF"),
                            contentPadding: const EdgeInsets.fromLTRB(20,0, 20, 0),
                            labelStyle: TextStyle(color: HexColor("#707070")),
                            errorBorder: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(3.0),
                              ),
                              borderSide: BorderSide(
                                color: Colors.red,
                              ),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(3.0),
                              ),
                              borderSide: BorderSide(
                                color: Colors.red,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(3.0),
                              ),
                              borderSide: BorderSide(color: HexColor("#FFFFFF")),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(3.0),
                              ),
                              borderSide: BorderSide(
                                color: HexColor("#FFFFFF"),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 20,),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: TextFormField(
                          controller: email,
                          onChanged: ((String password) {
                            setState(() {
                              _email = password;
                            });
                          }),
                          focusNode: focus,
                          validator: (value) {
                            if (value.isEmpty) {
                              return "";
                            }
                            if (value.length < 6) {
                              return " ";
                            } else
                              return null;
                          },
                          //obscureText: true,
                          autofocus: false,
                          decoration: InputDecoration(
                            hintText: "Select Area",
                            hintStyle: TextStyle(fontSize: 14),
                            prefixIcon: Icon(Icons.location_on_outlined,color: HexColor('#dd3f36'),),
                            filled: true,
                            fillColor: HexColor("#FFFFFF"),
                            contentPadding: const EdgeInsets.fromLTRB(20,0, 20, 0),
                            labelStyle: TextStyle(color: HexColor("#707070")),
                            errorBorder: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(3.0),
                              ),
                              borderSide: BorderSide(
                                color: Colors.red,
                              ),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(3.0),
                              ),
                              borderSide: BorderSide(
                                color: Colors.red,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(3.0),
                              ),
                              borderSide: BorderSide(color: HexColor("#FFFFFF")),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(3.0),
                              ),
                              borderSide: BorderSide(
                                color: HexColor("#FFFFFF"),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 20,),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: TextFormField(
                          controller: email,
                          onChanged: ((String password) {
                            setState(() {
                              _email = password;
                            });
                          }),
                          focusNode: focus,
                          validator: (value) {
                            if (value.isEmpty) {
                              return "";
                            }
                            if (value.length < 6) {
                              return " ";
                            } else
                              return null;
                          },
                          //obscureText: true,
                          autofocus: false,
                          decoration: InputDecoration(
                            hintText: "Select Date",
                            hintStyle: TextStyle(fontSize: 14),
                            prefixIcon: Icon(Icons.calendar_today,color: HexColor('#dd3f36'),),
                            filled: true,
                            fillColor: HexColor("#FFFFFF"),
                            contentPadding: const EdgeInsets.fromLTRB(20,0, 20, 0),
                            labelStyle: TextStyle(color: HexColor("#707070")),
                            errorBorder: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(3.0),
                              ),
                              borderSide: BorderSide(
                                color: Colors.red,
                              ),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(3.0),
                              ),
                              borderSide: BorderSide(
                                color: Colors.red,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(3.0),
                              ),
                              borderSide: BorderSide(color: HexColor("#FFFFFF")),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(3.0),
                              ),
                              borderSide: BorderSide(
                                color: HexColor("#FFFFFF"),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 20,),
                      Container(
                        alignment: Alignment.center,
                        width: width,
                        height: 50,
                        decoration: BoxDecoration(
                          color: HexColor('#dd3f36'),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Text("Search",style: TextStyle(
                            color: Colors.white
                        ),),
                      ),
                      SizedBox(height: 30,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("All Specialities"),
                          Icon(Icons.subject,color: HexColor('#dd3f36'),)
                        ],
                      ),
                      SizedBox(height: 10,),
                      ListView.builder(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: 8,
                          itemBuilder: (context,index){
                            return  Container(
                              padding: EdgeInsets.only(bottom: 20),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                          width: 90,
                                          height: 80,
                                          child: Image.asset("assets/images/doctor.jpg",fit: BoxFit.fitWidth,)),
                                      SizedBox(width: 8,),
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('Dr.Mathin Pillir',style: TextStyle(color: Colors.black,fontSize: 16),),
                                          Text('Dentist',style: TextStyle(color: Colors.grey,fontSize: 14),),
                                          Text('Franches',style: TextStyle(color: Colors.grey,fontSize: 14),),
                                          RatingBar.builder(
                                            unratedColor: Colors.white,
                                            itemSize: 12,
                                            initialRating: 3,
                                            minRating: 1,
                                            direction: Axis.horizontal,
                                            allowHalfRating: true,
                                            itemCount: 5,
                                            itemPadding: EdgeInsets.symmetric(horizontal: 0.5),
                                            itemBuilder: (context, _) => Icon(
                                              Icons.star,
                                              color: Colors.amber,
                                            ),
                                            onRatingUpdate: (rating) {
                                              print(rating);
                                            },
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                  Icon(Icons.wrap_text)
                                ],
                              ),
                            );
                          })

                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
