import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class MedicalResult extends StatefulWidget {
  const MedicalResult({Key key}) : super(key: key);

  @override
  _MedicalResultState createState() => _MedicalResultState();
}

class _MedicalResultState extends State<MedicalResult> {
  double height, width;

  @override
  Widget build(BuildContext context) {
    if (MediaQuery.of(context).orientation == Orientation.portrait) {
      height = MediaQuery.of(context).size.height;
      width = MediaQuery.of(context).size.width;
    } else {
      height = MediaQuery.of(context).size.width;
      width = MediaQuery.of(context).size.height;
    }

    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            ListView.builder(
             // physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: 15,
              itemBuilder: (context,index){
                return Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 6,),
                      Text("09/04/2021",style: TextStyle(color: Colors.grey,fontSize: 12),),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Text("Magnetect - claro",style: TextStyle(fontWeight: FontWeight.bold),),

                            ],
                          ),
                          Row(
                            children: [
                              Icon(Icons.arrow_downward_outlined,color: HexColor('#dd3f36'),),
                              SizedBox(width: 4,),
                              Text("Download",style: TextStyle(color: HexColor('#dd3f36')
                                  ,fontWeight: FontWeight.bold),),
                            ],
                          )
                        ],
                      ),
                      Divider(
                        thickness: 1,
                        color: Colors.grey,
                      ),
                      SizedBox(height: 6,),
                    ],
                  ),
                );
              },),
            SizedBox(height: 20,),
            Container(
              alignment: Alignment.center,
              width: width,
              height: 50,
              decoration: BoxDecoration(
                color: HexColor('#dd3f36'),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Text("Book a new request",style: TextStyle(
                  color: Colors.white
              ),),
            )
          ],
        ),
      ),
    );
  }
}
