import 'package:flutter/material.dart';
import 'package:karnak_scan_project/HomePages/appointment.dart';
import 'package:karnak_scan_project/HomePages/home2.dart';
import 'package:karnak_scan_project/HomePages/profile.dart';
import 'package:karnak_scan_project/HomePages/result.dart';
import 'package:karnak_scan_project/home.dart';


var baseColor = Color(0xffECF1FA); //backgroundColor
var redColor = Color(0xffE23C3C); //redColor
var bluecolor = Color(0xff181461); //blueColor

// ignore: non_constant_identifier_names
// ignore: must_be_immutable

class MyBottomNavigationBar extends StatefulWidget {
  MyBottomNavigationBar({Key key}) : super(key: key);

  @override
  _MyBottomNavigationBarState createState() => _MyBottomNavigationBarState();
}

class _MyBottomNavigationBarState extends State<MyBottomNavigationBar> {
  int _currentIndex = 0;

  final List<Widget> _children =
  [
    Home(),
    Appointment(),
    HomePage(),
    Profile(),

  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(

        currentIndex: _currentIndex,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
            backgroundColor: redColor,

          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Appointment',
            backgroundColor: redColor,
          ),


          BottomNavigationBarItem(
            icon: Icon(Icons.report),
            label: 'Results',
            backgroundColor: redColor,
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.face),
            label: 'Profile',
            backgroundColor: redColor,
          ),

        ],
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
      ),

    );
  }
}

class HomeScreenLess
    extends StatelessWidget {
  const HomeScreenLess
      ({Key key}) : super(key: key);

  @override

  Widget build(BuildContext context) {
    return MaterialApp(

      home: MyBottomNavigationBar(),

    );
  }
}