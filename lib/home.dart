import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:karnak_scan_project/book_appointment/book_appointment.dart';
import 'package:karnak_scan_project/heart_rate/heart_rate.dart';
import 'package:karnak_scan_project/medical_record/medical_home.dart';
import 'package:karnak_scan_project/x_ray/x_ray_home.dart';

import 'forum_discussions/forum_discussions.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  double height, width;

  @override
  Widget build(BuildContext context) {
    if (MediaQuery.of(context).orientation == Orientation.portrait) {
      height = MediaQuery.of(context).size.height;
      width = MediaQuery.of(context).size.width;
    } else {
      height = MediaQuery.of(context).size.width;
      width = MediaQuery.of(context).size.height;
    }

    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            SizedBox(height: height * 0.2,),
            GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute
                  (builder: (context)=>XRayHome()));
              },
              child: Container(
                alignment: Alignment.center,
                width: width,
                height: 50,
                decoration: BoxDecoration(
                  color: HexColor('#dd3f36'),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text("X-Ray",style: TextStyle(
                    color: Colors.white
                ),),
              ),
            ),
            SizedBox(height: 20,),
            GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute
                  (builder: (context)=>MedicalRecordHome()));
              },
              child: Container(
                alignment: Alignment.center,
                width: width,
                height: 50,
                decoration: BoxDecoration(
                  color: HexColor('#dd3f36'),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text("Mediacal Record",style: TextStyle(
                    color: Colors.white
                ),),
              ),
            ),
            SizedBox(height: 20,),
            GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute
                  (builder: (context)=>ForumDiscussuins()));
              },
              child: Container(
                alignment: Alignment.center,
                width: width,
                height: 50,
                decoration: BoxDecoration(
                  color: HexColor('#dd3f36'),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text("Forum",style: TextStyle(
                    color: Colors.white
                ),),
              ),
            ),
            SizedBox(height: 20,),
            GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute
                  (builder: (context)=>BookAppointment()));
              },
              child: Container(
                alignment: Alignment.center,
                width: width,
                height: 50,
                decoration: BoxDecoration(
                  color: HexColor('#dd3f36'),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text("Book appointment",style: TextStyle(
                    color: Colors.white
                ),),
              ),
            ),
            SizedBox(height: 20,),
            GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute
                  (builder: (context)=>HeartRate()));
              },
              child: Container(
                alignment: Alignment.center,
                width: width,
                height: 50,
                decoration: BoxDecoration(
                  color: HexColor('#dd3f36'),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text("Heart rate",style: TextStyle(
                    color: Colors.white
                ),),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
