import 'package:flutter/material.dart';
import 'package:karnak_scan_project/home.dart';
import 'package:karnak_scan_project/introPages/splashScreen.dart';
import 'package:karnak_scan_project/x_ray/x_ray_home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SplashScreen(),
    );
  }
}

