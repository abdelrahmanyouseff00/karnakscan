import 'package:flutter/material.dart';
import 'package:karnak_scan_project/auth_screen/login_screen.dart';
import 'package:karnak_scan_project/home_pages.dart';

class Verfication extends StatefulWidget {
  const Verfication({Key key}) : super(key: key);

  @override
  _VerficationState createState() => _VerficationState();
}

class _VerficationState extends State<Verfication> {

  var baseColor = Color(0xffECF1FA); //backgroundColor
  var redColor = Color(0xffE23C3C); //redColor
  var bluecolor = Color(0xff181461); //blueColor

// ignore: non_constant_identifier_names
  final _OTPController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      backgroundColor: baseColor,
      body: Stack(
        children: [

          //Sub Title
          Container(
              margin: EdgeInsets.only(top: 70, left: 105),
              height: 150.0,
              width: 150.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                      'assets/images/logo-v2.png'),
                  fit: BoxFit.fill,
                ),
              )

          ),

          //MobileNumber
          Container(
              color: baseColor,
              margin: EdgeInsets.only(top: 300, left: 120,),
              child: Text.rich(
                  TextSpan(children: <TextSpan>[
                    TextSpan(
                      text: "Verfication",
                      style: TextStyle(color: bluecolor, fontSize: 24),
                    ),

                  ])
              )
          ),

          Container(
              color: baseColor,
              margin: EdgeInsets.only(top: 340, left: 95),
              child: Text.rich(
                  TextSpan(children: <TextSpan>[
                    TextSpan(
                      text: "Please Insert your OTP code Here",
                      style: TextStyle(color: bluecolor, fontSize: 11),
                    ),

                  ])
              )
          ),

          Container(
            margin: EdgeInsets.only(top: 380),
            child: ListView(
              padding: EdgeInsets.all(32),

              children: [
                buildMobile(),
              ],
            ),

          ),

          //Continue Button
          Container(
            // ignore: deprecated_member_use
            child:RaisedButton(


              shape: RoundedRectangleBorder(
                  side: BorderSide(color: baseColor, width: 0),
                  borderRadius: BorderRadius.circular(18.0)
              ),

              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context)=> MyBottomNavigationBar()),
                );
              },
              color: redColor,
              child: Text("Continue",style:  TextStyle(color: Colors.white),),
            ),
            height: 43,
            color: baseColor,
            margin: EdgeInsets.only(top:500, left: 30),
            width: 300,

          ),

          Container(
            // ignore: deprecated_member_use
            child:RaisedButton(
              elevation: 0,
              hoverElevation: 0,
              focusElevation: 0,
              highlightElevation: 0,
              shape: RoundedRectangleBorder(
                side: BorderSide(color: baseColor, width: 0),
                borderRadius: BorderRadius.circular(0.0),

              ),

              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context)=>LoginScreen()),
                );
              },
              color: baseColor,
              child: Text("Change Number",style:  TextStyle(color: redColor, fontSize: 11)),
            ),
            height: 43,
            color: baseColor,
            margin: EdgeInsets.only(top:545, left: 220),
            width: 120,

          ),

          Container(
            // ignore: deprecated_member_use
            child:RaisedButton(
              elevation: 0,
              hoverElevation: 0,
              focusElevation: 0,
              highlightElevation: 0,
              shape: RoundedRectangleBorder(
                side: BorderSide(color: baseColor, width: 0),
                borderRadius: BorderRadius.circular(0.0),

              ),

              onPressed: () {
                // Navigator.push(context,
                // MaterialPageRoute(builder: (context)=>Verfication()),
                // );
              },
              color: baseColor,
              child: Text("Resent Code",style:  TextStyle(color: redColor, fontSize: 11)),
            ),
            height: 43,
            color: baseColor,
            margin: EdgeInsets.only(top:545, left: 10),
            width: 120,

          ),


          //TextMobileNumber

        ],
      ),

    );
  }
  Widget buildMobile() => TextField(
    controller: _OTPController,

    decoration: InputDecoration(

        labelText: "Insert your code here"
    ),
    keyboardType: TextInputType.phone,
    textInputAction: TextInputAction.done,
  );
}
