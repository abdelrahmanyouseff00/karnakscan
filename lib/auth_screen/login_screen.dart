import 'package:flutter/material.dart';
import 'package:karnak_scan_project/auth_screen/verfication.dart';

enum MobileVerficationState {
  SHOW_MOBILE_FORM_STATE,
  SHOW_OTP_FORM_STATE,
}

var baseColor = Color(0xffECF1FA); //backgroundColor
var redColor = Color(0xffE23C3C); //redColor
var bluecolor = Color(0xff181461); //b

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  final _controller = TextEditingController();

  final currentState = MobileVerficationState.SHOW_MOBILE_FORM_STATE;

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      backgroundColor: baseColor,
      body: Stack(
        children: [

          //Sub Title
          Container(
              margin: EdgeInsets.only(top: 70, left: 105),
              height: 150.0,
              width: 150.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                      'assets/images/logo-v2.png'),
                  fit: BoxFit.fill,
                ),
              )

          ),

          //MobileNumber
          Container(
              color: baseColor,
              margin: EdgeInsets.only(top: 300, left: 95,),
              child: Text.rich(
                  TextSpan(children: <TextSpan>[
                    TextSpan(
                      text: "Mobile Number",
                      style: TextStyle(color: bluecolor, fontSize: 24),
                    ),

                  ])
              )
          ),
          //the code will be sent to the full mobile number
          Container(
              color: baseColor,
              margin: EdgeInsets.only(top: 340, left: 60,),
              child: Text.rich(
                  TextSpan(children: <TextSpan>[
                    TextSpan(
                      text: "The code will be sent to the full mobile number",
                      style: TextStyle(color: bluecolor, fontSize: 11),
                    ),

                  ])
              )
          ),

          Container(
            margin: EdgeInsets.only(top: 380),
            child: ListView(
              padding: EdgeInsets.all(32),

              children: [
                buildMobile(),
              ],
            ),

          ),



          //Continue Button
          Container(


            // ignore: deprecated_member_use
            child:RaisedButton(
              shape: RoundedRectangleBorder(
                  side: BorderSide(color: baseColor, width: 0),
                  borderRadius: BorderRadius.circular(18.0)
              ),

              onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context)=>Verfication()),
                );
                print("Mobile number is " + _controller.text);
              },
              color: redColor,
              child: Text("Continue",style:  TextStyle(color: Colors.white),),
            ),
            height: 43,
            color: baseColor,
            margin: EdgeInsets.only(top:500, left: 30),
            width: 300,

          ),


          //TextMobileNumber

        ],
      ),

    );
  }
  Widget buildMobile() => TextField(

    controller: _controller,
    decoration: InputDecoration(

      labelText: "Phone Number",
      prefixIcon: Icon(Icons.phone),
      suffixIcon: Icon(Icons.close),

      border: OutlineInputBorder(
        borderRadius: const BorderRadius.all(
          const Radius.circular(30.0),
        ),
      ),
      prefixText: '+20',



    ),

    keyboardType: TextInputType.phone,
    maxLength: 10,
    textInputAction: TextInputAction.done,
  );
}
