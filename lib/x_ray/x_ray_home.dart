import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:karnak_scan_project/x_ray/x_ray_result.dart';
import 'package:tab_indicator_styler/tab_indicator_styler.dart';

class XRayHome extends StatefulWidget {
  const XRayHome({Key key}) : super(key: key);

  @override
  _XRayHomeState createState() => _XRayHomeState();
}

class _XRayHomeState extends State<XRayHome> {

  double height, width;

  final focus = FocusNode();

  final TextEditingController email = TextEditingController();

  String _email;

  @override
  Widget build(BuildContext context) {
    if (MediaQuery.of(context).orientation == Orientation.portrait) {
      height = MediaQuery.of(context).size.height;
      width = MediaQuery.of(context).size.width;
    } else {
      height = MediaQuery.of(context).size.width;
      width = MediaQuery.of(context).size.height;
    }
    return Scaffold(
      appBar: AppBar(
        actions: [
          Icon(Icons.person,color: HexColor('#a25d60'),),
          SizedBox(width: 10,)
        ],
        backgroundColor: HexColor('#edf1fc'),
        elevation: 0.0,
        iconTheme: IconThemeData(
          color: HexColor('#a25d60'), //change your color here
        ),
      ),
      body: Container(
        color: HexColor('#edf1fc'),
        padding: EdgeInsets.symmetric(horizontal: 16,vertical: 16),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  child: Text("X-ray Results",style: TextStyle(
                    fontWeight: FontWeight.bold
                  ),)),
              SizedBox(height: 10,),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                child: TextFormField(
                  controller: email,
                  onChanged: ((String password) {
                    setState(() {
                      _email = password;
                    });
                  }),
                  focusNode: focus,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "";
                    }
                    if (value.length < 6) {
                      return " ";
                    } else
                      return null;
                  },
                  //obscureText: true,
                  autofocus: false,
                  decoration: InputDecoration(
                    hintText: "Search",
                    hintStyle: TextStyle(fontSize: 14),
                    suffixIcon: Icon(Icons.search,color: Colors.grey,),
                    filled: true,
                    fillColor: HexColor("#FFFFFF"),
                    contentPadding: const EdgeInsets.fromLTRB(20,0, 20, 0),
                    labelStyle: TextStyle(color: HexColor("#707070")),
                    errorBorder: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(3.0),
                      ),
                      borderSide: BorderSide(
                        color: Colors.red,
                      ),
                    ),
                    focusedErrorBorder: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(3.0),
                      ),
                      borderSide: BorderSide(
                        color: Colors.red,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(3.0),
                      ),
                      borderSide: BorderSide(color: HexColor("#FFFFFF")),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(3.0),
                      ),
                      borderSide: BorderSide(
                        color: HexColor("#FFFFFF"),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10,),
              SingleChildScrollView(
                physics: const NeverScrollableScrollPhysics(),
                child: Container(
                  width: width,
                  height: height,
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: DefaultTabController(
                    length: 2,
                    child: NestedScrollView(
                        physics: const NeverScrollableScrollPhysics(),
                        headerSliverBuilder: (context, innerBoxScrolled) => [
                          SliverAppBar(
                            elevation: 0,
                            title: TabBar(
                              indicatorColor: Colors.grey,
                              labelColor: HexColor('#dd3f36'),
                              unselectedLabelColor: Colors.black,
                              indicator: MaterialIndicator(
                                color: HexColor('#dd3f36'),
                                // bottomLeftRadius: 100,
                                // bottomRightRadius: 100,
                                // topLeftRadius: 100,
                                // topRightRadius: 100,
                                //paintingStyle: PaintingStyle.fill,
                              ),
                              tabs: [
                                Tab(
                                  child: Container(

                                    child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                            "Recently",style: TextStyle(fontSize: 10)
                                        )),
                                  ),
                                ),
                                Tab(
                                  child: Container(
                                    child: Align(
                                        alignment: Alignment.centerRight,
                                        child: Text(
                                            "Past",style: TextStyle(fontSize: 10)
                                        )),
                                  ),
                                ),

                              ],
                            ),
                            //centerTitle: true,
                            automaticallyImplyLeading: false,
                            backgroundColor: Colors.transparent,
                            expandedHeight: 50,
                            pinned: false,
                          )
                        ],
                        body: TabBarView(
                          children: [
                            XRayResult(),
                            XRayResult()
                          ],
                        )),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
