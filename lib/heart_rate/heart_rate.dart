import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class HeartRate extends StatefulWidget {
  const HeartRate({Key key}) : super(key: key);

  @override
  _HeartRateState createState() => _HeartRateState();
}

class _HeartRateState extends State<HeartRate> {
  double height, width;

  int _selectitem = 0;
  List<String> dateList = ["D","W","M","Y"];

  @override
  Widget build(BuildContext context) {
    if (MediaQuery.of(context).orientation == Orientation.portrait) {
      height = MediaQuery.of(context).size.height;
      width = MediaQuery.of(context).size.width;
    } else {
      height = MediaQuery.of(context).size.width;
      width = MediaQuery.of(context).size.height;
    }

    return Scaffold(
      appBar: AppBar(
        actions: [
          Icon(Icons.person,color: HexColor('#a25d60'),),
          SizedBox(width: 10,)
        ],
        backgroundColor: HexColor('#edf1fc'),
        elevation: 0.0,
        iconTheme: IconThemeData(
          color: HexColor('#a25d60'), //change your color here
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 16,vertical: 16),
        color: HexColor('#edf1fc'),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Heart Rate",style: TextStyle(fontSize: 16
                  ,fontWeight: FontWeight.bold),),
              SizedBox(height: 4,),
              ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: 3,
                  itemBuilder: (context,index){
                return Container(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Rest Heart Rate Avarage",style:
                      TextStyle(fontSize: 14,color: HexColor('#a25d60'))),
                      SizedBox(height: 8,),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text('61',style:
                          TextStyle(fontSize: 24,)),
                          Text('BPM')
                        ],
                      ),
                      SizedBox(height: 8,),
                      Text("May 2019 - May 2020",style:
                      TextStyle(fontSize: 14)),
                      SizedBox(height: 20,),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: HexColor('#dce5f6'),
                        ),
                        //padding: EdgeInsets.symmetric(vertical: 4),
                        alignment: Alignment.center,
                        width: width,
                        height: 35,
                        child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                            itemCount: dateList.length,
                            itemBuilder: (context,index){
                              return GestureDetector(
                                onTap: (){
                                  setState(() {
                                    _selectitem = index;
                                  });
                                },
                                child: Container(
                                  width: 50,
                                  height: 30,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: _selectitem == index ? Colors.white :HexColor('#dce5f6'),
                                  ),
                                  alignment: Alignment.center,
                                  margin: EdgeInsets.symmetric(horizontal: width*0.046,vertical: 3),
                                  child: Text(dateList[index]),
                                ),
                              );
                            }),
                      ),
                      SizedBox(height: 10,),
                      Container(
                        width: width,
                        height: height * 0.4,
                        child: Image.asset("assets/images/doctor.jpg",fit: BoxFit.fitWidth,),
                      )
                    ],
                  ),
                );
              })
            ],
          ),
        ),
      ),
    );
  }
}
